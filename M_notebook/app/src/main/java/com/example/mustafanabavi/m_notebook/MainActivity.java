package com.example.mustafanabavi.m_notebook;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_add , btn_show;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_add = (Button)findViewById(R.id.button_add) ;
        btn_show = (Button)findViewById(R.id.button_show);

        btn_add.setOnClickListener(this);
        btn_show.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

      int btn_id =   v.getId();

        if(btn_id == btn_add.getId() ){

            intent = new Intent(MainActivity.this,Register_activity.class);
            startActivity(intent);
        }

        if(btn_id == btn_show.getId()){

            Intent inten = new Intent(MainActivity.this,Listview_activity.class);
            startActivity(inten);

        }
    }
}
