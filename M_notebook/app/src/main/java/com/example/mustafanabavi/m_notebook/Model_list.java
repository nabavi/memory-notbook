package com.example.mustafanabavi.m_notebook;

import java.util.ArrayList;

/**
 * Created by Mustafa Nabavi on 4/15/2017.
 */

public class Model_list {


    private String title ;
    private String time;
    private String date;
    private String phon;
    private String location;
    private String description;
    private int id;
    static int counter = 0 ;

    public  Model_list (){

    }
    public  Model_list ( int id , String title , String time, String date, String phon, String location, String description){


        this.id = id;
        this.title = title;
        this.time = time;
        this.date = date;
        this.phon = phon;
        this.location = location;
        this.description = description;

        counter = counter++;

    }

    public int getId(){

        return  id;
    }

    public void  setId (int id ){
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhon() {
        return phon;
    }

    public void setPhon(String phon) {
        this.phon = phon;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
