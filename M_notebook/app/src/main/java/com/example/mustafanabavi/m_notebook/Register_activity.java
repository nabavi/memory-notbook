package com.example.mustafanabavi.m_notebook;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Mustafa Nabavi on 4/15/2017.
 */

public class Register_activity extends Activity  implements View.OnClickListener{


    public  Register_activity(){

    }
    ListView listView;
    static ArrayList  arrayList ;
    Button btn_register, btn_reset ,btn_back ;

    EditText text_title, text_time, text_date, text_phon, text_location, text_descrip;

    Model_list model_list  = new Model_list();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);


        //Button contents
        btn_register = (Button)findViewById(R.id.button_register);
        btn_reset = (Button)findViewById(R.id.button_reset);
        btn_back = (Button)findViewById(R.id.button_back);


        text_title = (EditText)findViewById(R.id.editText_title);
        text_time = (EditText)findViewById(R.id.editText_time);
        text_date = (EditText)findViewById(R.id.editText_date);
        text_phon = (EditText)findViewById(R.id.editText_phon);
        text_location = (EditText)findViewById(R.id.editText_locat);
        text_descrip = (EditText)findViewById(R.id.editText_descrip);

        //my Array list initialize
        arrayList = new ArrayList();

        showTime();
        showDate();
      // button registrations
       btn_register.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_back.setOnClickListener(this);

    }

    public void addList(){


        arrayList.add(new Model_list(model_list.getId(),model_list.getTitle(),model_list.getTime(),model_list.getDate()
        ,model_list.getPhon(),model_list.getLocation(),model_list.getDescription()));



    }


    public  void showTime(){

        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        int am_pm = cal.get(Calendar.AM_PM);
        text_time.setText(hour+":"+minute+":"+second+" :"+am_pm);


    }

    public void showDate(){

        Calendar cal = Calendar.getInstance();
       int day =  cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        text_date.setText(year+"-"+month+"-"+day);


    }

    @Override
    public void onClick(View v) {

       int button_id =  v.getId();
        if(button_id == btn_register.getId()){


            model_list.setTitle(text_title.getText().toString());
            model_list.setTime(text_time.getText().toString());
            model_list.setDate(text_date.getText().toString());
            model_list.setPhon(text_phon.getText().toString());
            model_list.setLocation(text_location.getText().toString());
            model_list.setDescription(text_descrip.getText().toString());


            addList();

            Toast.makeText(Register_activity.this,"succes fully add"+model_list.getTitle(),Toast.LENGTH_SHORT).show();




        }


        else if(button_id == btn_reset.getId()){
           // showList();

        }
        else if(button_id == text_title.getId()){
            text_title.setText("");

        }

        else if(button_id == text_time.getId()){
          text_time.setText("");
        }
        else if(button_id == text_date.getId()){
            text_date.setText("");
        }

        else if(v.getId() == btn_back.getId()){

            Intent in = new Intent(Register_activity.this,MainActivity.class);
            startActivity(in);
        }
    }

    public void showList (){

        for (Object list : arrayList){

            Toast.makeText(Register_activity.this,list+"",Toast.LENGTH_SHORT).show();
        }
    }





}
