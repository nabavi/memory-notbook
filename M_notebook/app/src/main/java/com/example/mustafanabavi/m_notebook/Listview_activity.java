package com.example.mustafanabavi.m_notebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mustafa Nabavi on 4/15/2017.
 */

public class Listview_activity extends Activity {




    ListView lv;
    Button btn_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_listview);




        btn_home = (Button)findViewById(R.id.button_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Listview_activity.this,MainActivity.class);
                startActivity(intent);
            }
        });


        //this array pass to listview
        final ListView listview = (ListView) findViewById(R.id.my_listview);
        String[] values = new String[] { "Herat", "Kabul", "Takht safar",
                "Mazar", "sakhy", "party", "with family", "H .r",
                "with friends", "Nabavi", "Taraqy", "best find " };


Array_list l = new Array_list();
        ArrayList list1 = new ArrayList();
        new Array_list().getArrayList();
        final ArrayList<Object> list = new ArrayList<Object>();
        list1 = l.getArrayList();

        Log.d("myArray",list1.toString());
        for (int i = 0; i <values.length; ++i) {
            list.add(values[i]);
        }
        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                view.animate().setDuration(2000).alpha(0)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {


                                adapter.notifyDataSetChanged();
                                view.setAlpha(1);
                                Intent in = new Intent(Listview_activity.this,ShowResult_activity.class);
                                startActivity(in);
                            }
                        });
            }

        });



    }




        /*lv = (ListView) findViewById(R.id.my_listview);

        // Instanciating an array list (you don't need to do this,
        // you already have yours).
        List<String> your_array_list = new ArrayList<String>();
        your_array_list.add("My");
        your_array_list.add("bar");
        your_array_list.add("Kamal");

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                your_array_list );

        lv.setAdapter(arrayAdapter);

*/

    }
