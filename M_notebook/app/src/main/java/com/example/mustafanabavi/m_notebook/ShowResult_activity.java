package com.example.mustafanabavi.m_notebook;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Mustafa Nabavi on 4/17/2017.
 */

public class ShowResult_activity extends Activity {

    TextView txt_title, txt_time,txt_date, txt_phon , txt_location, txt_description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.show_result);

        txt_title = (TextView)findViewById(R.id.textView_title);
        txt_time = (TextView)findViewById(R.id.textView_Time);
        txt_date = (TextView)findViewById(R.id.textView_date);
        txt_phon = (TextView)findViewById(R.id.textView_phon);
        txt_location = (TextView)findViewById(R.id.textView_location);
        txt_description = (TextView)findViewById(R.id.textView_description);

    }
}
